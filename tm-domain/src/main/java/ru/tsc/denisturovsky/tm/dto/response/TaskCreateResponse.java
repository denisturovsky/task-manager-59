package ru.tsc.denisturovsky.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.dto.model.TaskDTO;

@NoArgsConstructor
public final class TaskCreateResponse extends AbstractTaskResponse {

    public TaskCreateResponse(@Nullable final TaskDTO task) {
        super(task);
    }

}
