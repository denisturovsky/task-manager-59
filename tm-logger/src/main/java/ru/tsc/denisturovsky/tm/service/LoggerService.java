package ru.tsc.denisturovsky.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import lombok.SneakyThrows;
import org.bson.Document;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.tsc.denisturovsky.tm.api.ILoggerPropertyService;

import java.util.LinkedHashMap;
import java.util.Map;


@Service
public final class LoggerService {

    @NotNull
    private final ObjectMapper objectMapper = new ObjectMapper();

    @NotNull
    private final ILoggerPropertyService propertyService = new LoggerPropertyService();

    @NotNull
    private final MongoClient mongoClient = new MongoClient(
            propertyService.getMDBServerHost(),
            Integer.parseInt(propertyService.getMDBServerPort())
    );

    @NotNull
    private final MongoDatabase mongoDatabase = mongoClient.getDatabase("tm_log");

    @SneakyThrows
    public void log(final String text) {
        @NotNull final Map<String, Object> event = objectMapper.readValue(text, LinkedHashMap.class);
        @NotNull final String table = event.get("table").toString();
        if (mongoDatabase.getCollection(table) == null) mongoDatabase.createCollection(table);
        final MongoCollection<Document> collection = mongoDatabase.getCollection(table);
        collection.insertOne(new Document(event));
    }

}