package ru.tsc.denisturovsky.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.tsc.denisturovsky.tm.api.repository.dto.ITaskDTORepository;
import ru.tsc.denisturovsky.tm.api.service.dto.IProjectDTOService;
import ru.tsc.denisturovsky.tm.api.service.dto.IUserDTOService;
import ru.tsc.denisturovsky.tm.comparator.NameComparator;
import ru.tsc.denisturovsky.tm.dto.model.TaskDTO;
import ru.tsc.denisturovsky.tm.dto.model.UserDTO;
import ru.tsc.denisturovsky.tm.marker.UnitCategory;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static ru.tsc.denisturovsky.tm.constant.ContextTestData.CONTEXT;
import static ru.tsc.denisturovsky.tm.constant.ProjectTestData.USER_PROJECT1;
import static ru.tsc.denisturovsky.tm.constant.TaskTestData.*;
import static ru.tsc.denisturovsky.tm.constant.UserTestData.USER_TEST_LOGIN;
import static ru.tsc.denisturovsky.tm.constant.UserTestData.USER_TEST_PASSWORD;

@Category(UnitCategory.class)
public final class TaskDTORepositoryTest {

    @NotNull
    private final static IUserDTOService USER_SERVICE = CONTEXT.getBean(IUserDTOService.class);

    @NotNull
    private final static IProjectDTOService PROJECT_SERVICE = CONTEXT.getBean(IProjectDTOService.class);

    @NotNull
    private static String USER_ID = "";

    @BeforeClass
    public static void setUp() throws Exception {
        @NotNull final UserDTO user = USER_SERVICE.create(USER_TEST_LOGIN, USER_TEST_PASSWORD);
        USER_ID = user.getId();
        PROJECT_SERVICE.add(USER_ID, USER_PROJECT1);
    }

    @AfterClass
    public static void tearDown() throws Exception {
        @Nullable final UserDTO user = USER_SERVICE.findByLogin(USER_TEST_LOGIN);
        PROJECT_SERVICE.remove(USER_ID, USER_PROJECT1);
        if (user != null) USER_SERVICE.remove(user);
    }

    @Test
    public void addByUserId() throws Exception {
        @NotNull final ITaskDTORepository repository = getRepository();
        Assert.assertNotNull(repository.add(USER_ID, USER_TASK3));
        @Nullable final TaskDTO task = repository.findOneById(USER_ID, USER_TASK3.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK3.getId(), task.getId());
        Assert.assertEquals(USER_ID, task.getUserId());
    }

    @After
    public void after() throws Exception {
        @NotNull final ITaskDTORepository repository = getRepository();
        repository.clear(USER_ID);
    }

    @Before
    public void before() throws Exception {
        @NotNull final ITaskDTORepository repository = getRepository();
        repository.add(USER_ID, USER_TASK1);
        repository.add(USER_ID, USER_TASK2);
    }

    @Test
    public void clearByUserId() throws Exception {
        @NotNull final ITaskDTORepository repository = getRepository();
        repository.clear(USER_ID);
        Assert.assertEquals(0, repository.getSize(USER_ID));
    }

    @Test
    public void createByUserId() throws Exception {
        @NotNull final ITaskDTORepository repository = getRepository();
        @NotNull final TaskDTO task = repository.create(USER_ID, USER_TASK3.getName());
        Assert.assertEquals(USER_TASK3.getName(), task.getName());
        Assert.assertEquals(USER_ID, task.getUserId());
    }

    @Test
    public void createByUserIdWithDescription() throws Exception {
        @NotNull final ITaskDTORepository repository = getRepository();
        @NotNull final TaskDTO task = repository.create(USER_ID, USER_TASK3.getName(), USER_TASK3.getDescription());
        Assert.assertEquals(USER_TASK3.getName(), task.getName());
        Assert.assertEquals(USER_TASK3.getDescription(), task.getDescription());
        Assert.assertEquals(USER_ID, task.getUserId());
    }

    @Test
    public void existsByIdByUserId() throws Exception {
        @NotNull final ITaskDTORepository repository = getRepository();
        Assert.assertFalse(repository.existsById(USER_ID, NON_EXISTING_TASK_ID));
        Assert.assertTrue(repository.existsById(USER_ID, USER_TASK1.getId()));
    }

    @Test
    public void findAllByUserId() throws Exception {
        @NotNull final ITaskDTORepository repository = getRepository();
        Assert.assertEquals(Collections.emptyList(), repository.findAll(""));
        final List<TaskDTO> tasks = repository.findAll(USER_ID);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
        tasks.forEach(task -> Assert.assertEquals(USER_ID, task.getUserId()));
    }

    @Test
    public void findAllComparatorByUserId() throws Exception {
        @NotNull final ITaskDTORepository repository = getRepository();
        @NotNull final Comparator comparator = NameComparator.INSTANCE;
        final List<TaskDTO> tasks = repository.findAll(USER_ID, comparator);
        Assert.assertNotNull(tasks);
        tasks.forEach(task -> Assert.assertEquals(USER_ID, task.getUserId()));
    }

    @Test
    public void findOneByIdByUserId() throws Exception {
        @NotNull final ITaskDTORepository repository = getRepository();
        Assert.assertNull(repository.findOneById(USER_ID, NON_EXISTING_TASK_ID));
        @Nullable final TaskDTO task = repository.findOneById(USER_ID, USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1.getId(), task.getId());
    }

    @NotNull
    public ITaskDTORepository getRepository() {
        return CONTEXT.getBean(ITaskDTORepository.class);
    }

    @Test
    public void getSizeByUserId() throws Exception {
        @NotNull final ITaskDTORepository repository = getRepository();
        Assert.assertEquals(2, repository.getSize(USER_ID));
    }

    @Test
    public void removeByUserId() throws Exception {
        @NotNull final ITaskDTORepository repository = getRepository();
        repository.remove(USER_ID, USER_TASK2);
        Assert.assertNull(repository.findOneById(USER_TASK2.getId()));
    }

    @Test
    public void update() throws Exception {
        @NotNull final ITaskDTORepository repository = getRepository();
        USER_TASK1.setName(USER_TASK3.getName());
        repository.update(USER_TASK1);
        Assert.assertEquals(USER_TASK3.getName(), repository.findOneById(USER_ID, USER_TASK1.getId()).getName());
    }

}