package ru.tsc.denisturovsky.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.dto.model.TaskDTO;

import java.util.Date;
import java.util.List;

public interface ITaskDTORepository extends IUserOwnedDTORepository<TaskDTO> {

    @NotNull
    TaskDTO create(
            @NotNull String userId,
            @NotNull String name,
            @NotNull String description
    ) throws Exception;

    @NotNull
    TaskDTO create(
            @NotNull String userId,
            @NotNull String name
    ) throws Exception;

    @NotNull
    TaskDTO create(
            @NotNull String userId,
            @NotNull String name,
            @NotNull String description,
            @NotNull Date dateBegin,
            @NotNull Date dateEnd
    ) throws Exception;


    @Nullable
    List<TaskDTO> findAllByProjectId(
            @NotNull String userId,
            @NotNull String projectId
    ) throws Exception;

}