package ru.tsc.denisturovsky.tm.sevice.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.denisturovsky.tm.api.repository.dto.ISessionDTORepository;
import ru.tsc.denisturovsky.tm.api.service.dto.ISessionDTOService;
import ru.tsc.denisturovsky.tm.dto.model.SessionDTO;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class SessionDTOService extends AbstractUserOwnedDTOService<SessionDTO, ISessionDTORepository> implements ISessionDTOService {

    @Nullable
    @Autowired
    private ISessionDTORepository repository;

    @Nullable
    protected ISessionDTORepository getRepository() {
        return repository;
    }

}