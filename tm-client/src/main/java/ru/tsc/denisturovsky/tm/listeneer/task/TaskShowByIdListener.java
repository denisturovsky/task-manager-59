package ru.tsc.denisturovsky.tm.listeneer.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.denisturovsky.tm.dto.model.TaskDTO;
import ru.tsc.denisturovsky.tm.dto.request.TaskShowByIdRequest;
import ru.tsc.denisturovsky.tm.event.ConsoleEvent;
import ru.tsc.denisturovsky.tm.util.TerminalUtil;

@Component
public final class TaskShowByIdListener extends AbstractTaskListener {

    @NotNull
    public static final String DESCRIPTION = "Show task by id";

    @NotNull
    public static final String NAME = "task-show-by-id";

    @Override
    @EventListener(condition = "@taskShowByIdListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskShowByIdRequest request = new TaskShowByIdRequest(getToken());
        request.setId(id);
        @Nullable final TaskDTO task = taskEndpoint.showByIdTask(request).getTask();
        showTask(task);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
